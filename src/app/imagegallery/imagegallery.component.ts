import { Component, OnInit } from '@angular/core';
import {Image} from '../image';
import {IMAGES} from '../mock-images';

@Component({
  selector: 'app-imagegallery',
  templateUrl: './imagegallery.component.html',
  styleUrls: ['./imagegallery.component.css']
})
export class ImagegalleryComponent implements OnInit {

  images = IMAGES;
  selectedImage: Image;

  constructor() { }

  ngOnInit() {
  }

  onSelect(image: Image): void {
    image.isFull = true;
    this.selectedImage = image;
  }

  onClear(): void {
    this.selectedImage.isFull = false;
    this.selectedImage = null;
  }

}
