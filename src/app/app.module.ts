import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ImagegalleryComponent } from './imagegallery/imagegallery.component';
import { ContactComponent } from './contact/contact.component';

import { JwSocialButtonsModule } from 'jw-angular-social-buttons';
import { ImagesComponent } from './images/images.component';

@NgModule({
  declarations: [
    AppComponent,
    ImagegalleryComponent,
    ContactComponent,
    ImagesComponent 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    JwSocialButtonsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
