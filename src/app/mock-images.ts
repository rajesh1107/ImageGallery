import {Image} from './image';

export const IMAGES: Image[] = [

	{id: 2, path: "https://www.tanishq.co.in/sites/all/themes/tanishq1/shubham/images/dp_se1.jpg", name: 'Deepika', isFull: false},

	{id: 3, path: "http://3.bp.blogspot.com/-NrYMcd3wD10/T3LfQzXIehI/AAAAAAAAB-U/8_KztM6UmnM/s1600/rani+mukherjee.1.jpg", name: 'Rani', isFull: false},

	{id: 4, path: "https://i9.dainikbhaskar.com/dainikbhaskar2010/logo/campaign_image20180702153003.jpg", name: 'Mahima', isFull: false},

	{id: 1, path: "https://cdn.dnaindia.com/sites/default/files/styles/full/public/2018/05/20/684360-alia-bhatt-insta.jpg", name: 'Alia', isFull: false},

	{id: 5, path: "https://www.naukrinama.com/stressbuster/wp-content/uploads/2017/06/prity-zinta.jpg", name: 'Prity', isFull: false},

	{id: 6, path: "https://c.tribune.com.pk/2016/11/1245297-RaveenaTandonwikiprofilemarriagebiographycopy-1480180360-486-640x480.JPG", name: 'Raveena', isFull: false},

	{id: 7, path: "https://www.dailypioneer.com/uploads/2018/story/images/big/anushka-sharma-promotes-tiger-conservation-2018-11-12.jpg", name: 'Anushka', isFull: false},

	{id: 8, path: "https://130513-387449-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2018/03/Anushka-Shetty.jpg", name: 'Anushka', isFull: false}


];