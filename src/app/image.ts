export class Image {
	id: number;
	path: string;
	name: string;
	isFull: boolean;
}