import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImagegalleryComponent }   from './imagegallery/imagegallery.component';

const routes: Routes = [
 { path: 'imagegallery', component: ImagegalleryComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
